var app = angular.module("simplexApp", []);

function TFunction(type, variables) {
  this.type = type;
  this.variables = variables;
}

TFunction.prototype.varKeys = function() {
  return Object.keys(this.variables);
}

TFunction.prototype.copy = function() {
  var variables = this.variables;
  return new TFunction(this.type, Object.keys(variables).reduce(function(obj, key) {
      obj[key] = variables[key];
      return obj;
    }, {}));
  }

function Restriction(type, variables, constant) {
  this.type = type;
  this.variables = variables;
  this.constant = constant;
}

Restriction.prototype.varKeys = function() {
  return Object.keys(this.variables);
}
Restriction.prototype.copy = function() {
  var variables = this.variables;
  return new TFunction(this.type, Object.keys(variables).reduce(function(obj, key) {
      obj[key] = variables[key];
      return obj;
    }, {}), this.constant);
  }

function Basis(tFunction, restrictions) {
  // get all posible variable keys
  var keys = restrictions.reduce(function(k, a) {
    return union(k, a.varKeys());
  }, []);
  this.functionValues = {};
  this.A0 = restrictions.map(function(a) {
    return a.constant;
  });
  this.vectors = {};
  for (var i = 0; i < keys.length; i++) {
    var bKey = keys[i].replace('x', 'A');
    this.functionValues[bKey] = tFunction.variables[keys[i]] ? tFunction.variables[keys[i]] : 0;
    this.vectors[bKey] = restrictions.map(function(res) {
      var value = res.variables[keys[i]] ? res.variables[keys[i]] : "0";
      return value;
    });
  }
  this.artBasis = {};
  for (var i = 0; i < restrictions.length; i++) {
    var key = "A(" + i + ")";
    this.artBasis[key] = [].splice(restrictions.length);
    this.artBasis[key][i] = 1;
  }
  this.basis = Object.keys(this.artBasis);
}

Basis.prototype.hasArtificial = function() {
  return Object.keys(this.artBasis).length != 0;
}

Basis.prototype.isArtificial = function(vecKey) {
  return Object.keys(this.artBasis).indexOf(vecKey) != -1;
}

function Estimation(basis) {
  var keys = union(Object.keys(basis.vectors), Object.keys(basis.artBasis));
  var real = keys.reduce(function(obj, a) {
    obj[a] = basis.artBasis[a] ? 0 : -basis.functionValues[a];
    return obj;
  }, {});
  real["A0"] = 0;
  if (basis.hasArtificial()) {
    console.log("Here");
    artificial = keys.reduce(function(obj, a) {
      obj[a] = basis.artBasis[a] ? 1 : 0;
      return obj;
    }, {});
    artificial["A0"] = 0;
  }
  basis.basis.forEach(function(vecKey, i) {
    if (basis.artBasis[vecKey]) {
      artificial["A0"] += - basis.A0[i];
    } else {
      real["A0"] += basis.functionValues[vecKey] * basis.A0[i];
    }
  });
  keys.forEach(function(key) {
    basis.basis.forEach(function(vecKey, i) {
      if (basis.artBasis[vecKey]) {
        // TODO:
        artificial[key] += (basis.artBasis[key]
                              ? (- basis.artBasis[key])
                              : (- basis.vectors[key]));
      } else {
        real[key] += basis.functionValues[vecKey] *
                        (basis.artBasis[key]
                              ? (- basis.artBasis[key])
                              : (- basis.vectors[key]));
        //(basis.functionValues[vecKey] * basis.vectors[key] - basis.functionValues[key]))
      }
    });
  });
  /*basis.basis.forEach(function(vecKey, i) {
    if (basis.artBasis[vecKey]) {
      artificial["A0"] += basis.functionValues[vecKey] * basis.A0[i];
    } else {
      real["A0"] += basis.functionValues[vecKey] * basis.A0[i];
    }
  });
  keys.forEach(function(key) {
    basis.basis.forEach(function(vecKey, i) {
      if (basis.artBasis[vecKey]) {
        artificial[key] += basis.functionValues[vecKey] * basis.artBasis[vecKey][i] + (basis.artBasis[key] ?) ;
      } else {
        real["A0"] += basis.functionValues[vecKey] * basis.vectors[vecKey][i];
      }
    });
  });*/
  this.real = real;
  if (artificial)
    this.artificial = artificial;
}

function SimplexTable(basis) {
  this.basis = basis;
  this.estimation = new Estimation(basis);
}


app.controller("SimplexController", ['$scope', function($scope) {


  $scope.varCount = 0;
  $scope.tFunction = new TFunction("min", {});
  $scope.restrictions = [];
  $scope.calculated = false;
  $scope.allAboveZero = true;
  $scope.canonical;
  $scope.tables = [];
  // TEST: (comment, when tested)
  $scope.varCount = 4;
  $scope.tFunction = new TFunction("min", {"x_1": -2, "x_2": 3, "x_3": -6, "x_4": -1});
  $scope.restrictions = [
    new Restriction("=", {"x_1": 2, "x_2": 1, "x_3": -2, "x_4": 1}, 24),
    new Restriction("<=", {"x_1": 1, "x_2": 2, "x_3": 4, "x_4": 0}, 22),
    new Restriction(">=", {"x_1": 1, "x_2": -1, "x_3": 2, "x_4": 0}, 10)
  ];

  $scope.number = function(value) {
    if (value)
      return value;
    else
      return "0";
  }
  $scope.keys = function(object) {
    return Object.keys(object);
  }
  $scope.varNames = function() {
    return Object.keys($scope.restrictions.variables);
  }
  $scope.invalidateVarCount = function() {
    var n = $scope.restrictions.length;
    if (n > $scope.varCount) {
      $scope.restrictions.splice($scope.varCount);
    } else {
      for (var i = n; i < $scope.varCount; i++) {
        addRestriction();
        addVariable("x_" + (i + 1));
      }
    }
  }
  $scope.calculate = function() {
    $scope.calculated = true;
    $scope.canonical = getCanonical($scope.tFunction, $scope.restrictions, $scope.allAboveZero);
    $scope.tables.push(new SimplexTable(new Basis($scope.canonical.tFunction, $scope.canonical.restrictions)));
    /*var result =
    $scope.tFunction = result.tFunction;
    $scope.restrictions = result.restrictions;*/
  }

  function addVariable(variable) {
    for (var i = 0; i < $scope.restrictions.length; i++)
      $scope.restrictions[i].variables[variable] = 0;
    $scope.tFunction.variables[variable] = 0;
  }
  function addRestriction() {
    var variables = {};
    for (var i = 0; i < $scope.varCount; i++)
      variables["x_" + (i + 1)] = 0;
    $scope.restrictions.push(new Restriction("=", variables, 0));
  }
}]);

function getCanonical(tFunction, restrictions, aboveZero) {
  var newTFunction = tFunction.copy();
  if (newTFunction.type == "min") {
    var keys = newTFunction.varKeys();
    for (var i = 0; i < keys.length; i++) {
      newTFunction.variables[keys[i]] = - newTFunction.variables[keys[i]];
    }
    newTFunction.type = "max";
  }

  var index = Object.keys(tFunction.variables).length + 1;
  var newRestrictions = restrictions.map(function(a) {
    var variables;
    if (aboveZero) {
      variables = Object.keys(a.variables).reduce(function(obj, b) { obj[b] = a.variables[b]; return obj; }, {});
    } else {
      variables = Object.keys(a.variables).reduce(function(obj, b) {
        obj[b + "\'"] = a.variables[b];
        obj[b + "\'\'"] = - a.variables[b];
        return obj;
      }, {});
    }
    if (a.type == ">=") {
      variables["x_" + (index++)] = -1;
    } else if (a.type == "<=") {
      variables["x_" + (index++)] = 1;
    }
    return new Restriction("=", variables, a.constant);
  });


  return {
    "tFunction": newTFunction,
    "restrictions" : newRestrictions
  }
}

function getBasisVectors(tFunction, restrictions) {


  return new Basis(functionValues, A0, vectors, artBasis);
}
function getInitTable() {

}

function union(arr1, arr2) {

  var result = arr1.slice();
  arr2.forEach(function(a) {
    if (arr1.indexOf(a) == -1)
      result.push(a);
  });
  console.log("[" + arr1 + "]" + "[" + arr2 + "] = [" + result + "]");
  return result;
}

app.directive("mathjaxBind", function() {
    return {
        restrict: "A",
        controller: ["$scope", "$element", "$attrs",
                function($scope, $element, $attrs) {
            $scope.$watch($attrs.mathjaxBind, function(texExpression) {
                var texScript = angular.element("<script type='math/tex'>")
                    .html(texExpression ? texExpression :  "");
                $element.html("");
                $element.append(texScript);
                MathJax.Hub.Queue(["Reprocess", MathJax.Hub, $element[0]]);
            });
        }]
    };
});
/*app.directive('mathjax', function() {
  return {
    restrict: 'EA',
    link: function(scope, element, attrs) {
      scope.$watch(attrs.ngModel, function() {
        MathJax.Hub.Queue(['Typeset', MathJax.Hub, element[0]]);
      })
    }
  };
});*/
