package takahawk.numberanalysis.slau

class SystemOfLinearEquations(val equations: Vector[LinearEquation]) {
  def this(equations: LinearEquation*) {
    this(equations.to[Vector])
    println(this.equations)
  }
  def toExtendedMatrix = new Matrix(equations.map(eq => eq.quotients ++ Vector(eq.constant)).to[Vector])

  def quotMatrix = new Matrix(equations.map(eq => eq.quotients).to[Vector])
  def constMatrix = new Matrix(equations.map(eq => Vector(eq.constant)).to[Vector])

  override def toString =
    equations.foldLeft(new StringBuilder)((str, s) => str.append(s + "\n")).toString
}
