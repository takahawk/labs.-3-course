package takahawk.numberanalysis.slau

class LinearEquation(val quotients: Vector[Double], val constant: Double) {
  override def toString = {
    (1 to quotients.length - 1).foldLeft(new StringBuilder())((str, i) =>
                  str.append(quotients(i - 1) + "x_" + i + " + ")).toString +
                  quotients.last + "x_" + quotients.length + " = " + constant
  }
}
