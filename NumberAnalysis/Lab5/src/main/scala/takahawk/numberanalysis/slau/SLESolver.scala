package takahawk.numberanalysis.slau

object SLESolver {
  val PRECISION = 0.0000001
  def zero(value:Double) = math.abs(value) < PRECISION

  def Gauss(system: SystemOfLinearEquations) = {
    def directFlow(matrix: Matrix) = {
      def transform(matrix: Matrix, i: Int, j: Int) = {
        println(matrix)
        val temp = matrix.rowMultiply(i, 1 / matrix.data(i)(j))
        (i + 1 to temp.data.length - 1).foldLeft(temp)((matr, k) =>
                matr.rowAddTimes(- matrix.data(k)(j))(i, k)
                /*matr.rowMultiply(k, - 1 / matrix.data(k)(j)).rowAdd(i, k)*/)
      }
      (0 to matrix.M - 1).foldLeft(matrix)((matr, i) =>   if (matr(i, i) != 0)
          transform(matr, i, i)
        else
          transform(matr.rowSwitch(i, (i to matr.M - 1).find(j => matr.data(j)(i) != 0).get), i, i))
    }
    def reverseFlow(matrix: Matrix) = {
      def transform(matrix: Matrix, i: Int, j: Int) = {
        val temp = matrix.rowMultiply(i, 1 / matrix.data(i)(j))
        (i - 1 to 0 by -1).foldLeft(temp)((matr, k) =>
              matr.rowAddTimes(- matrix.data(k)(j))(i, k)
              /*matr.rowMultiply(k, - 1 / matrix.data(k)(j)).rowAdd(i, k)*/)
      }
      (matrix.M - 1 to 0 by -1).foldLeft(matrix)(((matr, i) =>
            transform(matr, i, i)))
    }
    def getResult(matrix: Matrix) = {
      matrix.data.map(row => row.last).to[Vector]
    }
    val direct = directFlow(system.toExtendedMatrix)
    val reverse = reverseFlow(direct)
    val X = getResult(reverse)
    new GaussSolution(X, direct, reverse)
  }
  def matrixMethod(system: SystemOfLinearEquations) = {
    val A = system.quotMatrix
    val B = system.constMatrix
    val _A = A.inverseMatrix
    val X = _A * B
    new MatrixSolution(X.column(0), A, B, _A)
  }

  def detMethod(system: SystemOfLinearEquations) = {
    val matrix = system.quotMatrix
    val constColumn = system.constMatrix.column(0)
    val mainDet = matrix.det
    val (dets, xs) = matrix.data(0).indices.map(
      i => {
          val det = matrix.changeColumn(i)(constColumn).det
          (det, det / mainDet)
        }).to[Vector].unzip
    new DetSolution(xs, mainDet, dets)
  }
}
