package takahawk.numberanalysis.slau
import scala.io.Source

object Main {
  def main(args: Array[String]): Unit = {
    val filename = args(0)
    val system = new SystemOfLinearEquations(Source.fromFile(filename).getLines().map(s => {
      val temp = s.split(' ').map(str => str.toDouble)
      val (quot, const) = temp.splitAt(temp.length - 1)
      new LinearEquation(quot.to[Vector], const(0))
    }).to[Vector])
    println(system)
    println(SLESolver.Gauss(system))
    println(SLESolver.detMethod(system))
    println(SLESolver.matrixMethod(system))
  }
}
