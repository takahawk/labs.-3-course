package takahawk.numberanalysis.slau

class DetSolution(val X: Vector[Double], val mainDet: Double, val dets: Vector[Double]) {
  override def toString = {
    "DETERMINANT METHOD: \n------------------------\n" +
    "det = " + mainDet + "\n" +
    dets.indices.foldLeft(new StringBuilder)((str, i) => str.append("det_" + i + " = " + dets(i) + "\n")) +
    "Solution: " + X + "\n\n"
  }
}

class MatrixSolution(val X: Vector[Double], val A: Matrix, val B: Matrix, val _A: Matrix) {
  override def toString = {
    "MATRIX METHOD: \n------------------------\n" +
    "A: \n" + A + "\n\n" + "B: \n" + B + "\n\nA^-1: \n" + _A + "\n\nX: \n" + X + "\n\n"
  }
}

class GaussSolution(val X: Vector[Double], val directFlow: Matrix, val reverseFlow: Matrix) {
  override def toString = {
    "GAUSS METHOD: \n------------------------\n" +
    "Direct Flow: \n" + directFlow + "\n\nReverse Flow:\n" + reverseFlow + X + "\n\n"
  }
}
