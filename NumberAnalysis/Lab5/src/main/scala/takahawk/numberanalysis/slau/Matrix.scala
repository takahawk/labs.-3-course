package takahawk.numberanalysis.slau

class Matrix(val data: Vector[Vector[Double]]) {
  def apply(i: Int, j: Int) = data(i)(j)
  def apply(i: Int) = data(i)

  val M = data.length
  val N = data(0).length

  def rowSwitch(a: Int, b: Int) = {
      val first = math.min(a, b)
      val second = math.max(a, b)
      new Matrix(data.take(first) ++ Vector(data(second)) ++
            data.take(second).drop(first + 1) ++ Vector(data(first)) ++
          data.drop(second + 1))
    }

  def rowMultiply(row: Int, factor: Double) = {
    new Matrix(data.take(row) ++
        Vector(data(row).map(a => a * factor)) ++
        data.drop(row + 1))
  }

  def rowAdd(from: Int, to: Int) = rowAddTimes(1)(from, to)

  def rowAddTimes(times: Double)(from: Int, to: Int) = {
    new Matrix(data.take(to) ++
        Vector(data(to).indices.map(i => times * data(from)(i) + data(to)(i)).to[Vector]) ++
        data.drop(to + 1))
  }

  def column(number: Int) = data.indices.map(i => data(i)(number)).to[Vector]
  lazy val columns = data(0).indices.map(j =>
                  data.indices.map(i => data(i)(j)).to[Vector]).to[Vector]
  def row(number: Int) = data(number)

  def removeColumn(number: Int) =
    new Matrix(data.map(row => row.slice(0, number) ++
                        row.slice(number + 1, row.length)))

  def changeColumn(i: Int)(col: Vector[Double]) =
    new Matrix(data.indices.map(k => data(k).slice(0, i) ++
                                Vector(col(k)) ++
                                data(k).slice(i + 1, data(k).length)).
                                to[Vector])
  def removeRow(number: Int) = new Matrix(data.take(number) ++ data.drop(number + 1))
  def minor(i: Int, j: Int) = removeRow(i).removeColumn(j)
  def coMinor(i: Int, j: Int) =  minor(i, j).det

  lazy val det:Double = {
    if (data.length == 1)
      if (data(0).length == 1)
        data(0)(0)
      else
        throw new UnsupportedOperationException()
    else
      data(0).indices.foldLeft(0.0)((sum, i) => sum + data(0)(i) * complement(0, i))
  }

  def complement(i: Int, j: Int) = (if ((i + j) % 2 != 0) -1 else 1) * coMinor(i, j)
  def transpose = new Matrix(columns)
  def adjugateMatrix = {
    new Matrix(data.indices.map(i => data(0).indices.map(j =>
                complement(i, j)).to[Vector]).to[Vector]).transpose
  }
  def inverseMatrix = {
    new Matrix(adjugateMatrix.data.map(row => row.map(a => a / det)))
  }

  def *(that: Matrix): Matrix = {
  new Matrix(data.indices.map(i => that.data(0).indices.map(j =>
        data.indices.foldLeft(0.0)((sum, k) => sum + data(i)(k) * that.data(k)(j))
      ).to[Vector]).to[Vector])
    }

  override def toString = {
    data.foldLeft(new StringBuilder())((str, row) =>
          row.foldLeft(str)((str2, a) => str2.append(a + " ")).append("\n")
        ).toString
  }
  // def set(i: Int, j: Int)(value: Double) = new Matrix(data.take(i) ++ data(i).updated(j, value) ++ data.drop(i + 1))
}
