package takahawk.numberanalysis.slau

import org.scalatest._
import org.scalatest.Matchers._

class SLETest extends FlatSpec {
  val PRECISION = 0.001
  val testCase1 = new SystemOfLinearEquations(
        new LinearEquation(Vector(3.0, -2.0), -6.0),
        new LinearEquation(Vector(5.0, 1.0), 3.0)
        )
  val test1Result = Vector(0.0, 3.0)

  val testCase2 = new SystemOfLinearEquations(
        new LinearEquation(Vector(2, 1, 1), 2),
        new LinearEquation(Vector(1, -1, 0), -2),
        new LinearEquation(Vector(3, -1, 2), 2)
  )
  val testCase3 = new SystemOfLinearEquations(
        new LinearEquation(Vector(1, -1, 3, 1), 5),
        new LinearEquation(Vector(4, -1, 5, 4), 4),
        new LinearEquation(Vector(2, -2, 4, 1), 6),
        new LinearEquation(Vector(1, -4, 5, -1), 3)
  )
  val testCase2Matrix = new Matrix(
    Vector(Vector(2, 1, 1, 2),
    Vector(1, -1, 0, -2),
    Vector(3, -1, 2, 2))
  )
  val test2Result = Vector(-1.0, 1.0, 3.0)
  val test3Result = Vector(9.0, 18.0, 10.0, -16.0)

  def testVectors(expected: Vector[Double], real: Vector[Double]): Unit = {
    (0 to expected.length).map(i =>
      expected(0) should equal (real(0) +- PRECISION)
    )
  }
  "SystemOfLinearEquations" should "transform to extended matrix" in {
    assertResult(testCase2Matrix.data) {
      testCase2.toExtendedMatrix.data
    }
  }

  "SLESolver.Gauss" should "work with regular systems of 2 equations" in {
    testVectors(test1Result, SLESolver.Gauss(testCase1).X)
  }

  "SLESolver.detMethod" should "work with regular systems of 2 equations" in {
    assertResult(test1Result) {
      SLESolver.detMethod(testCase1).X
    }
  }

  "SLESolver.matrixMethod" should "work with regular systems of 2 equations" in {
    assertResult(test1Result) {
      SLESolver.matrixMethod(testCase1).X
    }
  }

  "SLESolver.Gauss" should "work with regular systems of 3 equations" in {
    testVectors(test2Result, SLESolver.Gauss(testCase2).X)
  }

  "SLESolver.detMethod" should "work with regular systems of 3 equations" in {
    assertResult(test2Result) {
      SLESolver.detMethod(testCase2).X
    }
  }

  "SLESolver.matrixMethod" should "work with regular systems of 3 equations" in {
    assertResult(test2Result) {
      SLESolver.matrixMethod(testCase2).X
    }
  }

  "SLESolver.Gauss" should "work with regular systems of 4 equations" in {
    testVectors(test3Result, SLESolver.Gauss(testCase3).X)
  }

  "SLESolver.detMethod" should "work with regular systems of 4 equations" in {
    assertResult(test3Result) {
      SLESolver.detMethod(testCase3).X
    }
  }

  "SLESolver.matrixMethod" should "work with regular systems of 4 equations" in {
    assertResult(test3Result) {
      SLESolver.matrixMethod(testCase3).X
    }
  }
}
