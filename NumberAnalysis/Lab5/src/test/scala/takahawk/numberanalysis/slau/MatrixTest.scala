package takahawk.numberanalysis.slau
import org.scalatest._
import org.scalatest.Matchers._

class MatrixTest extends FlatSpec {
  val PRECISION = 0.001
  val testVector1 = Vector(
          Vector(1.0, 2.0, 3.0),
          Vector(4.0, 5.0, 6.0),
          Vector(7.0, 8.0, 9.0)
  )
  val testVector2 = Vector(
    Vector(2.0, 1.0, 2.0),
    Vector(4.0, 6.0, 5.0),
    Vector(7.0, 8.0, 9.0)
  )
  val testVectorWithout1Row = Vector(
          Vector(4.0, 5.0, 6.0),
          Vector(7.0, 8.0, 9.0)
  )
  val testVectorWithout2Row = Vector(
          Vector(1.0, 2.0, 3.0),
          Vector(7.0, 8.0, 9.0)
  )
  val testVectorWithout1Column = Vector(
          Vector(2.0, 3.0),
          Vector(5.0, 6.0),
          Vector(8.0, 9.0)
  )
  val testVectorWithout2Column = Vector(
          Vector(1.0, 3.0),
          Vector(4.0, 6.0),
          Vector(7.0, 9.0)
  )
  val testVector1Minor22 = Vector(
          Vector(1.0, 3.0),
          Vector(7.0, 9.0)
  )
  val testVector1Transposed = Vector(
          Vector(1.0, 4.0, 7.0),
          Vector(2.0, 5.0, 8.0),
          Vector(3.0, 6.0, 9.0)
  )
  val test1Column1 = Vector(1.0, 4.0, 7.0)
  val test1NewColumn = Vector(12.0, 13.0, 14.0)
  val test1With1ColumnReplaced = Vector(
          Vector(12.0, 2.0, 3.0),
          Vector(13.0, 5.0, 6.0),
          Vector(14.0, 8.0, 9.0)
  )
  val test1mulTest2 = Vector(
    Vector(31, 37, 39),
    Vector(70, 82, 87),
    Vector(109, 127, 135)
  )
  val test2Adjugate = Vector(
    Vector(14, 7, -7),
    Vector(-1, 4, -2),
    Vector(-10, -9, 8)
  )
  val test2Inverse = Vector(
    Vector(2.0, 1.0, -1.0),
    Vector(-1.0 / 7.0, 4.0 / 7.0, -2.0 / 7.0),
    Vector(-10.0 / 7.0, - 9.0 / 7.0, 8.0 / 7.0)
  )
  val test1WithRow1SwitchedTo3 = Vector(
          Vector(7.0, 8.0, 9.0),
          Vector(4.0, 5.0, 6.0),
          Vector(1.0, 2.0, 3.0)
  )

  val test1WithRow2MultipliedBy2 = Vector(
          Vector(1.0, 2.0, 3.0),
          Vector(8.0, 10.0, 12.0),
          Vector(7.0, 8.0, 9.0)
  )
  val test1WithRow1AddedTo3 = Vector(
          Vector(1.0, 2.0, 3.0),
          Vector(4.0, 5.0, 6.0),
          Vector(8.0, 10.0, 12.0)
  )
  val test1Det = 0.0
  val test2Det = 7.0
  val testCase1 = new Matrix(testVector1)
  val testCase2 = new Matrix(testVector2)

  "Matrix" should "remove columns from data" in {
    assertResult(testVectorWithout1Column) {
      testCase1.removeColumn(0).data
    }
    assertResult(testVectorWithout2Column) {
      testCase1.removeColumn(1).data
    }
  }

  "Matrix" should "change columns" in {
    assertResult(test1With1ColumnReplaced) {
      testCase1.changeColumn(0)(test1NewColumn).data
    }
  }

  "Matrix" should "remove rows from data" in {
    assertResult(testVectorWithout1Row) {
      testCase1.removeRow(0).data
    }
    assertResult(testVectorWithout2Row) {
      testCase1.removeRow(1).data
    }
  }

  "Matrix" should "be able to give column" in {
    assertResult(test1Column1) {
      testCase1.column(0)
    }
  }
    "Matrix" should "evaluate minor" in {
      assertResult(testVector1Minor22) {
        testCase1.minor(1, 1).data
      }
    }
    "Matrix" should "evaluate determinant" in {
      new Matrix(Vector(Vector(1.0))).det should equal (1.0 +- PRECISION)
      testCase1.det should equal (test1Det +- PRECISION)
      testCase2.det should equal (test2Det +- PRECISION)
    }
    "Matrix" should "be able to transpose itself" in {
      assertResult(testVector1Transposed) {
        testCase1.transpose.data
      }
    }

    "Matrix" should "be able to get adjugate matrix of itself" in {
      assertResult(test2Adjugate) {
        testCase2.adjugateMatrix.data
      }
    }

    "Matrix" should "be able to inverse itself" in {
      assertResult(test2Inverse) {
        testCase2.inverseMatrix.data
      }
    }
    "Matrix" should "be able to be multiplied by another matrix" in {
      assertResult(test1mulTest2) {
        (testCase1 * testCase2).data
      }
    }

    "Matrix" should "be able to switch rows" in {
      assertResult(test1WithRow1SwitchedTo3) {
        testCase1.rowSwitch(0, 2).data
      }
    }

    "Matrix" should "be able to multiply rows" in {
      assertResult(test1WithRow2MultipliedBy2) {
        testCase1.rowMultiply(1, 2).data
      }
    }

    "Matrix" should "be able to add one row to another" in {
      assertResult(test1WithRow1AddedTo3) {
        testCase1.rowAdd(0, 2).data
      }
    }
}
